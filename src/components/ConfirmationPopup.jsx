import styles from "./confirmationPopup.module.css";

export function ConfirmationPopup(){
    return(
        <div className={styles.popup}>
            <div className={styles.popupTexts}>
                <p className={styles.text1}>
                    Confirmação de Reserva!
                </p>
                <p className={styles.text2}>
                    Tem certeza de que deseja confirmar a reserva?
                </p>
            </div>
            <div className={styles.buttons}>
                <button className={styles.cancelButton}>Cancelar</button>
                <button className={styles.confirmButton}>Confirmar</button>
            </div>
        </div>
    )
}