import styles from "./seatsInfo.module.css"

export function SeatsInfo(){
    return (
        <div className={styles.seatsInfo}>
            <p className={styles.seatID}>ID do assento</p>
            <hr className={styles.line}/>
            <div className={styles.nameWrapper}>
                <p className={styles.name}>Nome</p>
                <div>
                    <form>
                        <input type="text" className={styles.nameInput}/>
                    </form>
                </div>
            </div>
            <div className={styles.cpfWrapper}>
                <p className={styles.cpf}>CPF</p>
                <div>
                    <form>
                        <input type="text" className={styles.cpfInput}/>
                    </form>
                </div>
            </div>
        </div>
    )
}