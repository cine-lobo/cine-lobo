import styles from './header.module.css'
import { NavLink } from 'react-router-dom'
import iconeFilme from '../assets/Icone-Filmes.svg'
import iconeEntrar from '../assets/Icone-Entrar.svg'
import  iconeAjuda from '../assets/Icone-Ajuda.svg'
import logo from '../assets/Logo.svg'

export function Header() {
    return(
        <div className={styles.header}>    
            <img src={logo} alt="logo"/>
            <div className={styles.nav}>
                <NavLink to="/filmes">
                    <img src={iconeFilme} alt="" />
                </NavLink>
                <NavLink to="/login">
                    <img src={iconeEntrar} alt="" />
                </NavLink>
                <NavLink to="/cadastro">
                    <img src={iconeAjuda} alt="" />
                </NavLink>
            </div>
        </div>
    )
}