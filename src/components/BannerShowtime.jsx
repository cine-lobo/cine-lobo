import styles from './bannershowtime.module.css'
import classif from '../assets/class.svg'

export function BannerShowtime(props){
    console.log(props.filme)
    return(
        <main>
        <div className={styles.bannerShowtimes}>
            <div className={styles.bannerPoster}>
                <img src={props.filme.url} />
            </div>
            <div className={styles.movieInfo}>
                <div className={styles.movieTitle}>
                    <p className={styles.titleShowtimes}>{props.filme.titulo}</p>
                    <img src={classif} alt="" />
                </div>
                <div className={styles.movieDescription}>
                    <p className={styles.movieGenre}>{props.filme.genero}</p>
                    <p className={styles.movieSynopsis}>{props.filme.sinopse}</p>
                </div>
                <div className={styles.filters}>
                <select className={styles.dropbtn}>
                        <option value="">Cidade</option>
                        <option value="rio de janeiro">Rio de Janeiro</option>
                        <option value="niteroi">Niterói</option>
                        <option value="marica">Maricá</option>
                        <option value="sao goncalo">São Gonçalo</option>
                    </select>
                    <select className={styles.dropbtn}>
                        <option value="">Bairro</option>
                        <option value="barra">Barra da Tijuca, RJ</option>
                        <option value="leblon">Leblon, RJ</option>
                        <option value="icarai">Icaraí, Niterói</option>
                        <option value="alcantara">Alcântara, SG</option>
                        <option value="itaipuacu">Itaipuaçu, Maricá</option>
                    </select>
                </div>
            </div>
        </div>
        </main>
    )
}
