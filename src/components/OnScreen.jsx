import styles from './onscreen.module.css'

export function OnScreen(props){
    function handleRedirect(e, id) {
        e.preventDefault();
        console.log(id);
        window.location.assign(`http://localhost:5173/sessoes/${id}`);
      }
    return(
        <div className={styles.onScreen}>
        
        <div className={styles.onScreenMovies}>
            
            <div className={styles.cardMovie}>
                <img src={props.filme.url} className={styles.cardPhoto} />
                <p>{props.filme.titulo}</p>
                <button className={styles.cardButton} onClick={(e) => {
                    handleRedirect(e, props.filme.filmeId)}}>SESSÕES DISPONÍVEIS</button>
            </div>
            
        </div>
    </div>
    )
}