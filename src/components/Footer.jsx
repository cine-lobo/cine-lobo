import { NavLink } from 'react-router-dom'
import styles from './footer.module.css'
import iconeInsta from '../assets/Icone-Instagram.svg'
import iconeFace from '../assets/Icone-Facebook.svg'
import iconeLinkedin from '../assets/Icone-Linkedin.svg'
import INgresso from '../assets/INgresso.svg'


export function Footer() {
    return(
        <div className={styles.footer}>
        
            <div className={styles.upper_footer_container}>
                <div className={styles.info}>
                    <div className={styles.adress}>
                        <h2>Endereço</h2>
                        <p>Av. Milton Tavares de Souza,
                        s/n - Sala 115 B - Boa Viagem,
                        Niterói - RJ
                        CEP: 24210-315
                        </p>
                    </div>
                    <div className={styles.sac}>
                        <h2>
                            Fale Conosco
                        </h2>
                        <p>
                            contato@injunior.com.br
                        </p>
                    </div>
                    <div className={styles.icon}>
                        <a href="https://www.instagram.com/injunioruff/"><img src={iconeInsta} alt=""/></a>
                        <a href="https://www.facebook.com/injunioruff/"><img src={iconeFace} alt=""/></a>
                        <a href="https://br.linkedin.com/in/injunioruff"><img src={iconeLinkedin} alt=""/></a>
                    </div>
                </div>
                <div className={styles.footerlogo}>
                    <img src={INgresso} alt=""/>
                </div>
                <div className={styles.mapouter}>
                    <div className={styles.gmap_canvas}><iframe className={styles.gmap_iframe} frameBorder="0" scrolling="no" marginHeight="0"
                        marginWidth="0"
                        src="https://maps.google.com/maps?width=400&amp;height=400&amp;hl=en&amp;q=Av. Milton Tavares de Souza,                     s/n - Sala 115 B - Boa Viagem,                     Niterói - RJ                     CEP: 24210-315&amp;t=&amp;z=18&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe><a
                        href="https://gachanox.io/">Gacha Nox</a>
                    </div>
                </div>
            </div>    
            <div className={styles.copyright}>
                <p>© Copyright 2023. IN Junior. Todos os direitos reservados. Niterói, Brasil.</p>
            </div>

        </div>
    )
}