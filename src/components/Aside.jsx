import { SeatsInfo } from './SeatsInfo'
import { ConfirmationPopup } from './ConfirmationPopup'
import styles from './aside.module.css'
import { useParams } from 'react-router-dom'
import { useState, useEffect } from 'react'

export function Aside({id, filmeId, tipo, horario}){
    // const {id, filmeId, tipo, horario} = useParams()
    const [filme, setFilme] = useState({})

    useEffect(() => {
        const url = `http://localhost:3333/filmes/${filmeId}`
        const config = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }

        fetch(url, config)
        .then(response => response.json())
        .then(data => {
            setFilme(data);
            console.log(data);
        })
        .catch(error => {
            console.error('Erro ao obter as sessões:', error);
        });
    }, []);

    return(
        <div className={styles.aside}>
            <div className={styles.topWrapper}>
                <img src={filme.url} className={styles.moviePoster}/>
                <div className={styles.movieInfo}>
                    <p>{filme.titulo}</p>
                    <div className={styles.sessionWrapper}>
                        <div className={styles.sessionInfo}> {tipo} </div>
                        <div className={styles.sessionInfo}> {horario} </div>
                    </div>
                </div>
            </div>
            <div className={styles.bottomWrapper}>
                <div className={styles.seatsTitle}>
                    <img src="src/assets/chair.svg" alt="" className={styles.seat}/>
                    <p className={styles.chosenSeats}>ASSENTOS ESCOLHIDOS</p>
                </div>
                <div className={styles.seatsFrame}>
                    <SeatsInfo />
                </div>
                <button className={styles.confirmButton}>CONFIRMAR</button>
            </div>
            
        </div> 
    )
}