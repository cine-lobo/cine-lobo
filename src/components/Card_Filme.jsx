import { useEffect } from 'react'
import styles from './cardFilme.module.css'
import { useParams } from 'react-router-dom';


export function Card_Filme(props){

    function handleRedirect(e, id) {
        e.preventDefault();
        console.log(id);
        window.location.assign(`http://localhost:5173/sessoes/${id}`);
      }
     
    return(
        <div className={styles.movieCard} key={props.filme.filmeId}>
            <div className={styles.movie}>
                <div className={styles.cardPhoto}>
                    <img src={props.filme.url}/>
                </div>
                <div className={styles.cardTitle}>
                    <p>{props.filme.titulo}</p>
                    <img src="src/assets/class.svg" alt="" /> 
                </div>
                <div className={styles.description}>
                    <p>{props.filme.genero}</p>
                    <p>{props.filme.diretor}</p>
                    <p>{props.filme.sinopse}</p>
                </div> 
                <div className={styles.buttonDiv}>
                <button className={styles.cardButton} onClick={(e) => {
                    handleRedirect(e,props.filme.filmeId)
                }}>VER SESSÕES</button>
                </div>
            </div>
        </div>
    )
}

