import styles from './carousel.module.css'

export function Carousel(){
    return(
        <div className={styles.container}>
            <button className={styles.left_button} src="src/assets/Left.svg">
                <img src="src/assets/Left.svg" alt="" />
            </button>
            <button className={styles.right_button} src="src/assets/Right.svg">
                <img src="src/assets/Right.svg" alt="" />
            </button>
            <div className={styles.gallery_wrapper}>
                <div className={styles.gallery}>
                    <img src="src/assets/Ticket.svg" alt=""  className={styles.ticket}/>
                    <img src="src/assets/Ticket2.svg" alt=""  className={styles.ticket}/>
                    <img src="src/assets/Ticket3.svg" alt=""  className={styles.ticket}/>
                </div>
            </div>
        </div>
    )
}