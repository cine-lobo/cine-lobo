import { Seat } from "../components/Seat";
import styles from "./row.module.css";

export function Row() {
    return (
        <div className={styles.row}>
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
            <Seat />
        </div>
    )
}