import styles from "./seat.module.css"

export function Seat() {
    return(
        <button className={styles.seat}></button>
    )
}

export function SeatSelected() {
    return(
        <button className={styles.seatSelected}></button>
    )
}

export function SeatPurchased() {
    return(
        <button className={styles.seatPurchased}></button>
    )
}