import { Card_Filme } from "../components/Card_Filme";
import styles from "./movies.module.css";
import { useState, useEffect,} from "react";


export function Movies() {

    const [filmes,setFilmes] = useState([])
    const [searchTerm, setSearchTerm] = useState("");
    useEffect(() => {
        fetch(`http://localhost:3333/filmes`, {
            method : "GET",
            headers : {
                "Content-Type" : "application/json" 
            },

        }).then((bruteData) => {
            bruteData.json().then((data) => {
                setFilmes(data)
            }).catch((error) => {
                console.log(error)
            })
        }).catch((err) => {
            console.log(err)
        })
    }, [])

    return (
        <main>
            <div className={styles.banner}>
                <form className={styles.searchBar}>
                    <input 
                        type="text" 
                        placeholder="Pesquisar filmes"
                        onChange={(event) => {
                            setSearchTerm(event.target.value);
                        }}
                    />
                    {/* NomedaAPI.filter((val) => {
                        if (searchTerm == "") {
                            return val
                        } else if (val.nome_do_filme.toLowerCase().includes(searchTerm.toLowerCase())) {
                            return val
                        }
                    }).map((val, key) => {
                        return (
                            <div className={user} key={key}>
                                <p>{<Card_Filme />}</p>
                            </div>
                        );
                    })}*/}
                    <button type="submit">
                        <img src="src/assets/search.svg" alt="" />
                    </button>
                </form>
                <div className={styles.filters}>
                    <select className={styles.dropbtn}>
                        <option value="">Gênero</option>
                        <option value="terror">Terror</option>
                        <option value="comedia">Comédia</option>
                        <option value="acao">Ação</option>
                        <option value="romance">Romance</option>
                        <option value="sci-fi">Sci-fi</option>
                    </select>
                    <select className={styles.dropbtn}>
                        <option value="">Classificação</option>
                        <option value="livre">Livre</option>
                        <option value="10">10</option>
                        <option value="12">12</option>
                        <option value="14">14</option>
                        <option value="16">16</option>
                        <option value="18">18</option>
                    </select>
                </div>
            </div>
            <div className={styles.mainContent}>
                <div className={styles.moviesText}>
                    <p>Filmes</p>
                </div>
                <div className={styles.cardsWrapper}>
                    {filmes.map((filme) => {
                        return <Card_Filme filme = {filme}/>
                    })}
                    
                </div>
                <div className={styles.pages}>
                    <div className={styles.pagination}>
                        <button className={styles.pageItem}><img src="src/assets/LeftVector.svg" alt="" /></button>
                        <button className={styles.active}>1</button>
                        <button className={styles.pageItem}>2</button>
                        <button className={styles.pageItem}>...</button>
                        <button className={styles.pageItem}>4</button>
                        <button className={styles.pageItem}>5</button>
                        <button className={styles.pageItem}><img src="src/assets/RightVector.svg" alt="" /></button>             
                    </div>
                </div>
            </div>
        </main>
    )
}

