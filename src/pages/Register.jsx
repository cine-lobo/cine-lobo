export function Register() {
    return (
        <main>
            <div className={styles.registerWelcome}>
                <p>Junte-se à Comunidade Cinematográfica! Cadastre-se Aqui!</p>
                <p>Seja bem-vindo à nossa comunidade apaixonada pelo mundo do cinema. Ao fazer parte do nosso espaço digital, você está prestes a mergulhar em uma experiência cinematográfica única, onde a magia das telonas ganha vida com um toque moderno. Nosso formulário de cadastro é o primeiro passo para embarcar nessa jornada emocionante. Ao preenchê-lo, você se tornará um membro da nossa comunidade, onde amantes do cinema se reúnem para compartilhar o entusiasmo, as emoções e as histórias que permeiam cada cena.</p>
            </div>
            <div className={styles.registerForm}>
                <h1>Registre-se</h1>
                <form action="">
                    <textarea name="nome"  placeholder="Nome" />
                    <textarea name="nome"  placeholder="Sobrenome" />
                    <textarea name="nome"  placeholder="CPF" />
                    <textarea name="nome"  placeholder="Data de nascimento" />
                    <textarea name="nome"  placeholder="Nome de Usuário" />
                    <textarea name="nome"  placeholder="E-mail" />
                    <textarea name="mensagem"  placeholder="Senha" />
                    <textarea name="descricao"  placeholder="Confirmar Senha" />
                    <button>REGISTRAR</button>
                </form>
            </div>
        </main>
    )
}