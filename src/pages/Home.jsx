import styles from './home.module.css'
import { NavLink } from 'react-router-dom'
import { Carousel } from '../components/Carousel'
import { OnScreen } from '../components/OnScreen'
import { useState,useEffect } from "react";


export function Home() {

    const [filmes,setFilmes] = useState([])
    useEffect(() => {
        fetch(`http://localhost:3333/filmes`, {
            method : "GET",
            headers : {
                "Content-Type" : "application/json" 
            },

        }).then((bruteData) => {
            bruteData.json().then((data) => {
                setFilmes(data)
            }).catch((error) => {
                console.log(error)
            })
        }).catch((err) => {
            console.log(err)
        })
    }, [])
    return (
        
        <main>
            <div className={styles.content}>
                <div className={styles.intro}>
                    <img src="src/assets/Fundo.svg" alt="" />
                    <div className={styles.homePhrase}>
                        <p>Transformando Filmes em Experiências Personalizadas</p>
                        <p>Reserve Seu Assento e Viva a Magia do Cinema!</p>
                    </div>
                
                </div>
                <Carousel />
                <h2 className= {styles.onAir}>Em Cartaz</h2>
                <div className={styles.onscreenContainer}>
                {filmes.map((filme) => {
                        return <OnScreen filme = {filme}/>
                    })}
                </div>
            </div>
            <NavLink to="/filmes"><h1>Ver mais</h1></NavLink> 
        </main>                                
    )
}

