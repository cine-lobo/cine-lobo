import styles from "./contact.module.css"

export function Contact() {
    return (
        <main>
            <div className={styles.content}>
                <div className={styles.contactWrapper}>
                    <div className={styles.contactTexts}>
                        <p className={styles.text1}>Contato</p>
                        <p className={styles.text2}>
                        Encontrou algum problema?
                        <br/>
                        Envie uma mensagem!</p>  
                    </div>
                    <form className={styles.contactForm} action="" method="get">
                        <textarea className={styles.name} name="nome" value="" placeholder="Nome Completo" />
                        <textarea className={styles.topic} name="mensagem" value="" placeholder="Assunto" />
                        <textarea className={styles.desc} name="descricao" value="" placeholder="Descrição Detalhada" />
                        <button className={styles.sendBtn}>ENVIAR</button>
                    </form>
                </div>
            </div>
        </main>
    )
}