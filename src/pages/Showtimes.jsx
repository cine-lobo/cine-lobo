import { useEffect, useState } from "react";
import { BannerShowtime } from "../components/BannerShowtime";
import styles from "./showtimes.module.css"
import { useParams } from "react-router-dom";

export function Showtimes() {

    function handleRedirect(e, id, filmeId, tipo, horario) {
        e.preventDefault();
        console.log(id);
        
        window.location.assign(`http://localhost:5173/assentos?id=${id}&filmeId=${filmeId}&tipo=${tipo}&horario=${horario}`);
      }

    function renderSession(session, index) {
  return (
    <div key={index}>
      <div className={styles.tabName}>
        {session.tipo}
      </div>
      <div className={styles.showtimeButtons}>
        <button className={styles.showtimeButton}>
          {session.horario}
        </button>
      </div>
    </div>
  );
}

    const [filme, setFilme] = useState({})
    const[filteredSessions, setFilteredSessions] = useState([])
    const[session, setSessions] = useState([])
    const[selectedType, setSelectedType] = useState([])

    useEffect( () => {
        const url = `http://localhost:3333/sessoes/${id}`
        const config = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }

        fetch(url, config)
        .then(response => response.json())
        .then(data => {
            setSessions(data);
            setFilteredSessions(data);
        })
        .catch(error => {
            console.error('Erro ao obter as sessões:', error);
        });
        }, []);

    const handleFilter = (type) => {
        setSelectedType(type);
        if (type === '') {
            setFilteredSessions(sessions);
        } else {
        const filtered = sessions.filter(session => 
            session.tipo === type);
            setFilteredSessions(filtered);
            }
        };

    const {id} = useParams()

    useEffect(() => {
        const url = `http://localhost:3333/filmes/${id}`
        const config = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }

        fetch(url, config)
        .then(response => response.json())
        .then(data => {
            setFilme(data);
            console.log(data);
        })
        .catch(error => {
            console.error('Erro ao obter as sessões:', error);
        });
    }, []);


    return (
        <main>
            <BannerShowtime filme = {filme}/>
            <div className={styles.movieSessions}>
                <div className={styles.movieTabs}>
                    <button className={styles.sessionButton} onClick={() => handleFilter('2D')}>
                        2D
                    </button>
                    <button className={styles.sessionButton} onClick={() => handleFilter('3D')}>
                        3D
                    </button>
                    <button className={styles.sessionButton} onClick={() => handleFilter('IMAX')}>
                        IMAX
                    </button>
                </div>
                <div className={styles.showtimesContainer}>
                    <div className={styles.showtimes}>
                    {filteredSessions.map((session, index) => (	
                        <div key={index}>
                            <div className={styles.tabName}>
                                {session.tipo}
                            </div>
                            <div className={styles.showtimeButtons}>
                                <button className={styles.showtimeButton} onClick={(e) => handleRedirect(e, session.sessoesId, filme.filmeId, session.tipo, session.horario)}>
                                    {session.horario}
                                </button>
                            </div>
                        </div>
                        ))}
                    </div>
                </div>

            </div>
    </main>
    )
}