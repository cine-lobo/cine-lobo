import { useEffect, useState } from "react";
import { Aside } from "../components/Aside";
import { Row } from "../components/Row";
import { Seat, SeatPurchased, SeatSelected } from "../components/Seat";
import styles from "./checkout.module.css";
import { useSearchParams } from "react-router-dom";
import tela from"../assets/tela.svg"

export function Checkout() {

    const [ searchParams, setSearchParams ] = useSearchParams();

    // const {id} = useSearchParams()
    const id = searchParams.get("id");
    const filmeId = searchParams.get("filmeId");
    const tipo = searchParams.get("tipo");
    const horario = searchParams.get("horario");
    const [sessao, setSessao] = useState({})

    console.log(filmeId)
    console.log(tipo)
    console.log(horario)
    
    console.log(id)

    useEffect(() => {
        const url = `http://localhost:3333/sessoes/${id}`
        const config = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }

        fetch(url, config)
        .then(response => response.json())
        .then(data => {
            setSessao(data);
            console.log(data);
        })
        .catch(error => {
            console.error('Erro ao obter as sessões:', error);
        });
    }, []);

    const [assentos, setAssentos] = useState([])

    useEffect(() => {
        const url = `http://localhost:3333/assentos/${sessao.sessoesId}`
        const config = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }
        fetch(url, config)
        .then(response => response.json())
        .then(data => {
            setAssentos(data);
            console.log(data);
        })
        .catch(error => {
            console.error('Erro ao obter as sessões:', error);
        });
    }, []);

    return (
        <main>
            <div className={styles.content}>
                <Aside id={id} filmeId = {filmeId} tipo={tipo} horario={horario} />
                <div className={styles.room}>
                    <img src={tela} alt=""  className={styles.screen}/>
                     
                    <div className={styles.rowContainer}>
                        <p>A</p>
                        <Row />
                        <p>A</p>
                    </div>
                    <div className={styles.rowContainer}>
                        <p>B</p>
                        <Row />
                        <p>B</p>
                    </div>
                    <div className={styles.rowContainer}>
                        <p>C</p>
                        <Row />
                        <p>C</p>
                    </div>
                    <div className={styles.rowContainer}>
                        <p>D</p>
                        <Row />
                        <p>D</p>
                    </div>
                    <div className={styles.rowContainer}>
                        <p>E</p>
                        <Row />
                        <p>E</p>
                    </div>
                    <div className={styles.rowContainer}>
                        <p>F</p>
                        <Row />
                        <p>F</p>
                    </div>
                    <div className={styles.rowContainer}>
                        <p>G</p>
                        <Row />
                        <p>G</p>
                    </div>
                    <div className={styles.rowContainer}>
                        <p>H</p>
                        <Row />
                        <p>H</p>
                    </div>
                    <div className={styles.rowContainer}>
                        <p>I</p>
                        <Row />
                        <p>I</p>
                    </div>
                    <div className={styles.rowContainer}>
                        <p>J</p>
                        <Row />
                        <p>J</p>
                    </div>
                    <hr className={styles.line}/>
                    <p className={styles.caption}>LEGENDA</p>
                    <div className={styles.captionStates}>
                        <div className={styles.state}>
                            <Seat />
                            <p>Disponível</p>
                        </div>
                        <div className={styles.state}>
                            <SeatSelected />
                            <p>Selecionado</p>
                        </div>
                        <div className={styles.state}>
                            <SeatPurchased />
                            <p>Comprado</p>
                        </div>        
                    </div>
                </div> 
            </div>
        </main>
    )
}