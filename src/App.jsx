import { BrowserRouter, Routes, Route } from 'react-router-dom'
import './global.css'
import { Home } from "./pages/Home.jsx"
import { Header } from "./components/Header"
import { Checkout } from "./pages/Checkout.jsx"
import { Contact } from "./pages/Contact.jsx"
import { Login } from "./pages/Login.jsx"
import { Movies } from './pages/Movies.jsx'
import { Register } from './pages/Register.jsx'
import { Showtimes } from './pages/Showtimes.jsx'
import { Footer } from './components/Footer.jsx'


export default function App() {
    return (
        <BrowserRouter>
            <Header />
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/assentos" element={<Checkout />} />
                <Route path="/contato" element={<Contact />} />
                <Route path="/login" element={<Login />} />
                <Route path="/filmes" element={<Movies />} />
                <Route path="/cadastro" element={<Register />} />
                <Route path="/sessoes/:id" element={<Showtimes />} />
            </Routes>
            <Footer />
        </BrowserRouter>
    )
}


